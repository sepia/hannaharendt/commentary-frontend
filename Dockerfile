ARG NODE_VERSION=16.17

FROM node:${NODE_VERSION} as dev

WORKDIR /app

VOLUME /app

CMD ["npm", "start"]

FROM node:${NODE_VERSION} as build

RUN npm install -g npm@8.17.0

WORKDIR /app

COPY package*.json ./

RUN npm ci --omit=dev --ignore-scripts

COPY src src
COPY public public

RUN npm run build

FROM nginx:1.22-alpine

LABEL \
    org.label-schema.dockerfile="/Dockerfile" \
    org.label-schema.license="AGPL-3.0-or-later" \
    org.label-schema.maintainer="Stefan Hynek" \
    org.label-schema.name="Hannah Arendt Commentary Frontend" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.url="https://www.arendteditionprojekt.de/" \
    org.label-schema.vcs-url="https://gitlab.gwdg.de/sepia/hannaharendt/commentary-frontend" \
    org.label-schema.vendor="SUB/FE"

COPY --from=build /app/build /usr/share/nginx/html

ARG build_date
ARG vcs_ref
ARG version
LABEL \
    org.label-schema.build-date="${build_date}" \
    org.label-schema.vcs-ref="${vcs_ref}" \
    org.label-schema.version="${version}"

COPY Dockerfile /
