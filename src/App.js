import "../node_modules/draft-js/dist/Draft.css";

import React from "react";
import { useTranslation } from "react-i18next";
import { Grid, Typography } from "@mui/material";

import LabTabs from "./components/LabTabs";
import SidePanel from "./components/SidePanel";
import { retrieveCommentaryIds } from "./api/crud";


export default function App() {
  const [editionId, setEditionId] = React.useState("");
  const [commentaryId, setCommentaryId] = React.useState("");

  const { t, i18n } = useTranslation();

  const handleOnChoseFile = async (editionId) => {
    // each editionId starts with a volume number
    const volumeNr =  editionId.split("-")[0];
    const commentaryIds = await retrieveCommentaryIds();
    const commentaryId = commentaryIds.find(element => element.startsWith(volumeNr + "-"));
    // is this safe to do?
    setCommentaryId(commentaryId);
    setEditionId(editionId);
  };

  return (
    <>
      <SidePanel CommentaryId={commentaryId} OnChoseFile={handleOnChoseFile} />
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="flex-start"
      >
        <Grid item xs={6}>
          {
            editionId
              ? <LabTabs CommentaryId={commentaryId} EditionId={editionId} key={editionId} />
              : <Typography component="p" sx={{ color: "secondary.main", fontSize: 34, fontWeight: "medium" }}>
                {t("openEdition.init")}
              </Typography>
          }
        </Grid>
      </Grid>
    </>
  );
}
