import axios from "axios";

import TimedoutError from "../errors/TimedoutError";
import ExistsError from "../errors/ExistsError";
import NotfoundError from "../errors/NotfoundError";

var BASE_URI;
if (process.env.NODE_ENV === "production"){
  BASE_URI = "/api";
} else {
  BASE_URI = "/exist/restxq";
}

const TIMEOUT = 1000;

const http = axios.create({
  baseURL: BASE_URI,
  timeout: TIMEOUT,
  headers: {
    "Content-Type" : "application/json;charset=utf-8",
    "Accept" : "application/json",
  },
});

//WARNING: axios does not set Content-Type header when request body is empty!! => %rest:consumes does not accept

//TODO: catch low level API errors with SENTRY



async function createNode (commentary, comment, note, node, data) {
  const resource = `/commentaries/${commentary}/comments/${comment}/notes/${note}?node=${node}`;
  try {
    // 2xx only, we can assume that there is a response then
    const response = await http.post(resource, data);
    return response.headers.location;
  } catch (error) {
    if (typeof error.response !== "undefined")
      throw new NotfoundError(resource);
    else
      throw new TimedoutError(TIMEOUT);
  }
}

/**
 *
 * @param {string} commentary commentary file
 * @param {string} comment comment id
 * @param {integer} note note number
 * @param {string} node node name
 * @returns
 */
async function retrieveNode (commentary, comment, note, node) {
  try {
    const response = await http.get(`/commentaries/${commentary}/comments/${comment}/notes/${note}?node=${node}`);
    return response.data;
  } catch (error) {
    console.error(error);
  }
}

/**
 * Directly update a node with a string value.
 *
 * @param {string} commentary commentary file
 * @param {string} comment comment id
 * @param {integer} note note number
 * @param {string} node node name
 * @param {string} data data to update node with
 */
async function updateNode (commentary, comment, note, node, data) {
  // design choice: the update request response will not return anything in its body
  // because the backend will not wait for the update operation to complete before answering
  try {
    await http.put(`/commentaries/${commentary}/comments/${comment}/notes/${note}?node=${node}`, data);
  } catch (error) {
    console.error(error);
  }
}

async function destroyNode (commentary, comment, note, node) {
  try {
    await http.delete(`/commentaries/${commentary}/comments/${comment}/notes/${note}?node=${node}`);
  } catch (error) {
    console.error(error);
  }
}


async function retrieveLanguages (edition) {
  try {
    const response = await http.get(`/editions${edition ? "/" + edition : ""}?select=language`);
    return response.data;
  } catch (error) {
    if (typeof error.response !== "undefined")
      //this should currently not occur due to missing error handling on API side but...who knows
      return [{}, [error.response.status, error.response.statusText]];
    else
      throw new TimedoutError(TIMEOUT, error.message);
  }
}

async function retrieveEditions () {
  try {
    const response = await http.get("/editions?select=id");
    return response.data;
  } catch (error) {
    if (typeof error.response !== "undefined")
      //this should currently not occur due to missing error handling on API side but...who knows
      return [{}, [error.response.status, error.response.statusText]];
    else
      throw new TimedoutError(TIMEOUT, error.message);
  }
}

async function retrieveEdition (editionId) {
  try {
    const response = await http.get(`/editions/${editionId}?select=id`);
    return response.data;
  } catch (error) {
    if (typeof error.response !== "undefined")
      //this should currently not occur due to missing error handling on API side but...who knows
      return [{}, [error.response.status, error.response.statusText]];
    else
      throw new TimedoutError(TIMEOUT, error.message);
  }
}

/**
 * Create a comment.
 * return ID
 */
async function createComment (commentary, data) {
  try {
    // 2xx only, we can assume that there is a response then
    const response = await http.post(`/commentaries/${commentary}/comments`, data);
    return response.headers.location;
  } catch (error) {
    if (typeof error.response !== "undefined") {
      // we have a response: it is not a timeout error!
      if (error.response.status === 409)
        throw new ExistsError(error.response.headers.location.split("/").pop());
      else
      {
        console.error(error.response.data);
        throw new Error(error.response.status + " " + error.response.statusText);
      }
    } else
      throw new TimedoutError(TIMEOUT);
  }
}

async function retrieveCommentaryIds () {
  const resource = "/commentaries?select=id";
  try {
    const response = await http.get(resource);
    return response.data;
  } catch (error) {
    if (typeof error.response !== "undefined")
      throw new NotfoundError(resource);
    else
      throw new TimedoutError(TIMEOUT);
  }
}

async function retrieveCommentaryFilename (commentary) {
  const resource = `/commentaries/${commentary}?select=filename`;
  try {
    const response = await http.get(resource);
    return response.data;
  } catch (error) {
    if (typeof error.response !== "undefined")
      throw new NotfoundError(resource);
    else
      throw new TimedoutError(TIMEOUT);
  }
}

async function retrieveComment (commentary, comment) {
  const resource = `/commentaries/${commentary}/comments/${comment}`;
  try {
    const response = await http.get(resource);
    return response.data;
  } catch (error) {
    if (typeof error.response !== "undefined")
      throw new NotfoundError(resource);
    else
      throw new TimedoutError(TIMEOUT);
  }
}

async function retrieveComments (commentary) {
  const resource = `/commentaries/${commentary}`;
  try {
    const response = await http.get(resource);
    return response.data;
  } catch (error) {
    if (typeof error.response !== "undefined")
      throw new NotfoundError(resource);
    else
      throw new TimedoutError(TIMEOUT);
  }
}

async function retrieveCommentsIds (commentary) {
  const resource = `/commentaries/${commentary}?select=id`;
  try {
    const response = await http.get(resource);
    return response.data;
  } catch (error) {
    if (typeof error.response !== "undefined")
      throw new NotfoundError(resource);
    else
      throw new TimedoutError(TIMEOUT);
  }
}


async function createNote (commentary, comment) {
  const resource = `/commentaries/${commentary}/comments/${comment}/notes`;
  try {
    // 2xx only, we can assume that there is a response then
    const response = await http.post(resource);
    return response.headers.location;
  } catch (error) {
    if (typeof error.response !== "undefined")
      throw new NotfoundError(resource);
    else
      throw new TimedoutError(TIMEOUT);
  }
}

async function retrieveNote (commentary, comment, note) {
  const resource = `/commentaries/${commentary}/comments/${comment}/notes/${note}`;
  try {
    const response = await http.get(resource);
    return response.data;
  } catch (error) {
    if (typeof error.response !== "undefined")
      throw new NotfoundError(resource);
    else
      throw new TimedoutError(TIMEOUT);
  }
}

async function destroyNote (commentary, comment, note) {
  const resource = `/commentaries/${commentary}/comments/${comment}/notes/${note}`;
  try {
    //currently always returns 200
    const response = await http.delete(resource);
    return response.data;
  } catch (error) {
    if (typeof error.response !== "undefined")
      throw new NotfoundError(resource);
    else
      throw new TimedoutError(TIMEOUT);
  }
}

async function retrieveNotes (commentary, comment) {
  const resource = `/commentaries/${commentary}/comments/${comment}/notes`;
  try {
    const response = await http.get(resource);
    return response.data;
  } catch (error) {
    if (typeof error.response !== "undefined")
      throw new NotfoundError(resource);
    else
      throw new TimedoutError(TIMEOUT);
  }
}



export { createComment, createNode, createNote, destroyNode, destroyNote, retrieveCommentaryIds, retrieveCommentaryFilename, retrieveComment, retrieveComments, retrieveCommentsIds, retrieveEdition, retrieveEditions, retrieveLanguages, retrieveNode, retrieveNote, retrieveNotes, updateNode };
