import React from "react";
import { useTranslation } from "react-i18next";

import PropTypes from "prop-types";

import { Button, TextField, LinearProgress, Tooltip } from "@mui/material";

import AddIcon from "@mui/icons-material/Add";

import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { ListItem, ListItemIcon, ListItemText } from "@mui/material";

import { waitUntil } from "async-wait-until";

import { useSnackbar } from "notistack";


import { createComment, retrieveComment } from "../api/crud";


AddComment.propTypes = {
  CommentaryId: PropTypes.string,
  IsDisabled: PropTypes.bool,
};

export default function AddComment(props) {
  const commentaryId = props.CommentaryId;
  const isDisabled = props.IsDisabled;

  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [value, setValue] = React.useState("");

  const { enqueueSnackbar } = useSnackbar();
  const { t } = useTranslation();



  const handleChange = (event) => setValue(event.target.value);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);



  const handleSubmit = async () => {
    setLoading(true);
    try {
      const location = await createComment(commentaryId, value);
      const commentId = location.split("/").pop(); //this is "substring-after-last"
      await waitUntil(
        //this is a function checking wether the comment exists in the backend
        async () => {
          try {
            await retrieveComment(commentaryId, commentId);
            return true;
          } catch {
            return false;
          }
        }
      );

      enqueueSnackbar(`Element ${commentId} created.`,
        { variant: "success" }
      );

      // TODO: view a newly created tab for the comment

    } catch (error) {
      enqueueSnackbar(error.message, {
        variant: "error",
      });
      return;
    } finally {
      setLoading(false);
    }
    setOpen(false);
  };

  return (
    <>
      <Tooltip title={t("comment.add.tooltip")}>
        <ListItem
          button
          disabled={isDisabled}
          key={"add"}
          aria-label="add comment"
          onClick={handleOpen}
        >
          <ListItemIcon>
            <AddIcon />
          </ListItemIcon>
          <ListItemText primary="Add comment" />
        </ListItem>
      </Tooltip>

      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">{t("comment.add.title")}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {t("comment.add.description")}
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            fullWidth
            disabled={loading}
            name="id"
            label={t("comment.add.inputLabel")}
            onChange={ handleChange }
            value={value}
            size="small"
          />
          {loading && <LinearProgress />}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            {t("button.cancel.title")}
          </Button>
          <Button disabled={loading} onClick={handleSubmit} color="primary" variant="contained">
            {t("comment.add.title")}
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );

}

