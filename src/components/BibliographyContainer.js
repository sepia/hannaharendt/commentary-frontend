import * as React from "react";

import PropTypes from "prop-types";

import {Stack, TextField, Tooltip} from "@mui/material";

import LinearProgress from "@mui/material/LinearProgress";


import { waitUntil } from "async-wait-until";
import { useSnackbar } from "notistack";


import  { createNode, destroyNode, retrieveNode, updateNode } from "../api/crud";
import { useTranslation } from "react-i18next";

BibliographyContainer.propTypes = {
  CommentaryId: PropTypes.string,
  CommentId: PropTypes.string,
  NoteId: PropTypes.number,
  Values: PropTypes.object,
};

export default function BibliographyContainer(props) {
  const commentaryId = props.CommentaryId;
  const commentId = props.CommentId;
  const noteId = props.NoteId;

  const [values, setValues] = React.useState(props.Values);
  const [loading, setLoading] = React.useState(false);
  const [changed, setChanged] = React.useState(false);

  const { enqueueSnackbar } = useSnackbar();
  const { t } = useTranslation();





  const handleChange = (event) => {
    /**
     * handle the state of multiple TextFields
     * TODO: track the "changed" status and saveState only "on changed"
     */
    setValues(prevState => ({ ...prevState, [event.target.name] : event.target.value }));
    setChanged(prevState => ({ ...prevState, [event.target.name] : true }));
  };

  //compare local state with remote state
  // this function is the same for all calls => REFACTOR
  const compare = async (data, node) => {
    const remote_state = await retrieveNode(commentaryId, commentId, noteId, node);
    console.debug(remote_state +"="+ data);
    return remote_state === data;
  };

  const saveState = async (event) => {
    const local_state = event.target.value;
    const targetNode = event.target.name;

    // dont save if nothing has changed, break function call instead
    if (! changed[targetNode]){
      //console.warn("NOT changed");
      return;
    }

    // something has changed... prepare async update!
    setLoading(prevState => ({ ...prevState, [targetNode] : true }));
    console.debug("local state: " + JSON.stringify(local_state));


    /**update only on change from existent value to another value; use create if empty; use delete if new state is empty */
    // updateNode(commentaryId, commentId, noteId, targetNode, local_state);

    if (local_state === "") {
      destroyNode(commentaryId, commentId, noteId, targetNode);
    } else {
      const remote_state = await retrieveNode(commentaryId, commentId, noteId, targetNode);
      if (remote_state === "")
        createNode(commentaryId, commentId, noteId, targetNode, local_state);
      else
        updateNode(commentaryId, commentId, noteId, targetNode, local_state);
    }



    try {
      //a single get call directly after post will sometimes be too fast (before the update operation has completed), so wait until the change has propagated but no longer than a set timeout
      const success = await waitUntil(() => compare(local_state, targetNode));
      console.debug("success? " + success);
      if (success) {
        // this is redundant!
        //const remote_state = await retrieveNode(commentaryId, commentId, noteId, targetNode);
        //console.debug("remote state: " + JSON.stringify(remote_state));
        //setValues(remote_state);
        setChanged(prevState => ({ ...prevState, [targetNode] : false }));
        enqueueSnackbar(`Node "${targetNode}" updated.`,
          { variant: "success" }
        );
      }
    } catch (error) {
      enqueueSnackbar(error.message, {
        variant: "error",
      });
      //TODO: provide a "repeat/retry" option
    } finally {
      setLoading(prevState => ({ ...prevState, [targetNode] : false }));
    }

  };

  return (
    // name ^= values."name"
    //TODO: this implementation is too WET (aka non-DRY)
    <Stack spacing={1}>
      <Tooltip title={t("bibContainer.tooltip.corresp")}>
        <TextField
          name="corresp"
          label={t("bibContainer.inputLabel.corresp")}
          value={values.corresp}
          onChange={handleChange}
          onBlur={saveState}
          size="small"
        />
      </Tooltip>
      {loading["corresp"] && <LinearProgress />}
      <Stack direction="row" spacing={1}>
        <Tooltip title={t("bibContainer.tooltip.from")}>
          <TextField
            name="from"
            label={t("bibContainer.inputLabel.from")}
            value={values.from}
            onChange={handleChange}
            onBlur={saveState}
            size="small"
          />
        </Tooltip>
        {loading["from"] && <LinearProgress />}
        <Tooltip title={t("bibContainer.tooltip.to")}>
          <TextField
            name="to"
            label={t("bibContainer.inputLabel.to")}
            value={values.to}
            onChange={handleChange}
            onBlur={saveState}
            size="small"
          />
        </Tooltip>
        {loading["to"] && <LinearProgress />}
        <Tooltip title={t("bibContainer.tooltip.n")}>
          <TextField
            name="n"
            label={t("bibContainer.inputLabel.n")}
            value={values.n}
            onChange={handleChange}
            onBlur={saveState}
            size="small"
          />
        </Tooltip>
        {loading["n"] && <LinearProgress />}
      </Stack>
      <Tooltip title={t("bibContainer.tooltip.scope")}>
        <TextField
          name="scope"
          label={t("bibContainer.inputLabel.scope")}
          value={values.scope}
          onChange={handleChange}
          onBlur={saveState}
          size="small"
        />
      </Tooltip>
      {loading["scope"] && <LinearProgress />}
      <Tooltip title={t("bibContainer.tooltip.reference")}>
        <TextField
          name="reference"
          label={t("bibContainer.inputLabel.reference")}
          value={values.reference}
          onChange={handleChange}
          onBlur={saveState}
          size="small"
        />
      </Tooltip>
      {loading["reference"] && <LinearProgress />}
      <Tooltip title={t("bibContainer.tooltip.internal")}>
        <TextField
          name="internal"
          label={t("bibContainer.inputLabel.internal")}
          value={values.internal}
          onChange={handleChange}
          onBlur={saveState}
          size="small"
        />
      </Tooltip>
      {loading["internal"] && <LinearProgress />}
    </Stack>
  );
}
