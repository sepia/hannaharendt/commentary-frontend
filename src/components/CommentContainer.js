import * as React from "react";

import PropTypes from "prop-types";

import { Fab, LinearProgress, Stack, Tooltip, Typography } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";

import { waitUntil } from "async-wait-until";
import { useSnackbar } from "notistack";

import LanguageDropdownBox from "./LanguageDropdownBox";
import RenderCheckBox from "./RenderCheckBox";
import BibliographyContainer from "./BibliographyContainer";
import CommentaryField from "./CommentaryField";

import { createNote, destroyNote, retrieveComment, retrieveNote } from "../api/crud";
import { useTranslation } from "react-i18next";

CommentContainer.propTypes = {
  CommentaryId: PropTypes.string,
  CommentId: PropTypes.string,
  Languages: PropTypes.objectOf(PropTypes.string),
};

export default function CommentContainer(props) {
  const commentaryId = props.CommentaryId;
  const commentId = props.CommentId;

  const [comment, setComment] = React.useState({ id: commentId, notes: [] });
  const [loading, setLoading] = React.useState(false);

  const { enqueueSnackbar } = useSnackbar();
  const { t } = useTranslation();

  React.useEffect(async () => {
    setLoading(true);
    try {
      setComment(await retrieveComment(commentaryId, commentId)); //props.Id ist beim ersten Render noch leer...?
    } catch (error) {
      enqueueSnackbar(error.message, {
        variant: "error",
      });
      return;
    } finally {
      setLoading(false);
    }
  }, []);

  const handleAddNote = async () => {
    // is this necessary? we have the prop commentId!
    const commentId = comment.id;

    setLoading(true);
    try {
      const location = await createNote(commentaryId, commentId);
      const noteNr = location.split("/").pop(); //this is "substring-after-last"
      // 2.b wait for object to be created: useInterval hook???
      await waitUntil(async () => {
        try {
          await retrieveNote(commentaryId, commentId, noteNr);
          return true;
        } catch {
          return false;
        }
      });

      enqueueSnackbar(`Note ${noteNr} created.`, { variant: "success" });
      // TODO: view the newly created note (=> rerender with setState?)
    } catch (error) {
      enqueueSnackbar(error.message, {
        variant: "error",
      });
      return;
    } finally {
      setComment(await retrieveComment(commentaryId, commentId));
      setLoading(false);
    }
  };

  const handleDeleteNote = async (noteNr) => {
    setLoading(true);
    try {
      await destroyNote(commentaryId, commentId, noteNr);
      enqueueSnackbar(`Note ${noteNr} deleted.`, { variant: "success" });
    } catch (error) {
      enqueueSnackbar(error.message, {
        variant: "error",
      });
      return;
    } finally {
      setComment(await retrieveComment(commentaryId, commentId));
      setLoading(false);
    }
  };

  return (
    <>
      <Typography variant="h1">{comment.id}</Typography>
      {loading && <LinearProgress />}
      <Stack spacing={4}>
        {Array.from(comment.notes).map((note, i) => (
          <Stack key={comment.id + i} spacing={1} sx={{ p: 2, border: "1px dashed grey" }}>
            <CommentaryField
              CommentaryId={commentaryId}
              CommentId={comment.id}
              Languages={props.Languages}
              NoteId={i}
              Paragraphs={note.comm}
            />

            <LanguageDropdownBox
              CommentaryId={commentaryId}
              CommentId={comment.id}
              NoteId={i}
              Languages={props.Languages}
              Value={note.lang}
            />

            <RenderCheckBox CommentaryId={commentaryId} CommentId={comment.id} NoteId={i} Value={note.rend} />

            <BibliographyContainer CommentaryId={commentaryId} CommentId={comment.id} NoteId={i} Values={note.bibl} />
            <Tooltip title={t("note.delete.tooltip")}>
              <Fab aria-label="delete note" onClick={(event) => handleDeleteNote(i, event)}>
                <RemoveIcon />
              </Fab>
            </Tooltip>
          </Stack>
        ))}
      </Stack>
      <Tooltip title={t("note.add.tooltip")}>
        <Fab aria-label="add note" disabled={loading} onClick={handleAddNote}>
          <AddIcon />
        </Fab>
      </Tooltip>
    </>
  );
}
