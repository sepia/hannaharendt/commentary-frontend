import React from "react";
import PropTypes from "prop-types";

import {CompositeDecorator, convertToRaw,  Editor, EditorState, RichUtils} from "draft-js";
import "../../node_modules/draft-js/dist/Draft.css";
import { convertFromHTML, convertToHTML } from "draft-convert";

import { Dialog, DialogActions, DialogContent, DialogTitle, TextField, ToggleButtonGroup, Tooltip } from "@mui/material";
import { Button, DialogContentText, Divider, FormControl, MenuItem, Select, Stack, ToggleButton } from "@mui/material";
import FormatBoldIcon from "@mui/icons-material/FormatBold";
import FormatItalicIcon from "@mui/icons-material/FormatItalic";
import FormatStrikethroughIcon from "@mui/icons-material/FormatStrikethrough";
import FormatUnderlinedIcon from "@mui/icons-material/FormatUnderlined";
import AddLinkIcon from "@mui/icons-material/AddLink";
import LinkOffIcon from "@mui/icons-material/LinkOff";
import { Box } from "@mui/system";
import { SaveAlt } from "@mui/icons-material";


import { findLinkEntities, Link } from "./draftjs/Link";
import { findLanguageEntities, Language } from "./draftjs/Language";
import { additionalDraftStyles } from "./draftjs/styles";

import { useTranslation } from "react-i18next";
import { updateNode, destroyNode } from "../api/crud";

const NODE_NAME = "comm";

// a decorator for draft entities
const compositeDecorator = new CompositeDecorator([
  {
    strategy: findLinkEntities,
    component: Link,
  },
  {
    strategy: findLanguageEntities,
    component: Language,
  },
]);

// map inline styles with their draftjs names
const inlineStyles = {
  bold: {draftName: "BOLD"},
  italic: {draftName: "ITALIC"},
  strikethrough: {draftName: "STRIKETHROUGH"},
  underline: {draftName: "UNDERLINE"},
};

// map for conversion from ContentState to "HTML"
const conversionMap = {
  styleToHTML: (style) => {
    if (style === "BOLD") {
      return <hi rend="bold" />;
    }
    if (style === "ITALIC") {
      return <hi rend="italic" />;
    }
    if (style === "STRIKETHROUGH") {
      return <hi rend="strikethrough" />;
    }
    if (style === "UNDERLINE") {
      return <hi rend="underline" />;
    }
  },
  entityToHTML: (entity, originalText) => {
    if (entity.type === "LINK") {
      return <a href={entity.data.url}>{originalText}</a>;
    }
    if (entity.type === "LANGUAGE") {
      return <span lang={entity.data.lang}>{originalText}</span>;
    }
    return originalText;
  }
};

// map for conversion from "HTML" to ContentState
const conversionMapReverse = {
  htmlToStyle: (nodeName, node, currentStyle) => {
    if (nodeName === "hi" && node.attributes.rend.nodeValue === "bold") {
      return currentStyle.add("BOLD");
    } else if (nodeName === "hi" && node.attributes.rend.nodeValue === "italic") {
      return currentStyle.add("ITALIC");
    } else if (nodeName === "hi" && node.attributes.rend.nodeValue === "strikethrough") {
      return currentStyle.add("STRIKETHROUGH");
    } else if (nodeName === "hi" && node.attributes.rend.nodeValue === "underline") {
      return currentStyle.add("UNDERLINE");
    } else {
      return currentStyle;
    }
  },
  htmlToEntity: (nodeName, node, createEntity) => {
    if (nodeName === "a") {
      return createEntity(
        "LINK",
        "MUTABLE",
        {url: node.href}
      );
    }
    if (nodeName === "span") {
      return createEntity(
        "LANGUAGE",
        "MUTABLE",
        {lang: node.lang}
      );
    }
  }
};

/** Take an array of paragraphs and convert it to a `ContentState`.
 * @param {Array.Array.<string>} paragraphs
 * @returns {ContentState} the draft editor content state
 */
function convert (paragraphs) {
  let pString = "";
  for (const p of paragraphs) {
    console.debug(p);
    pString += "<p>" + p.join("") + "</p>";
  }
  const contentState = convertFromHTML(
    conversionMapReverse)(
    pString
  );
  return contentState;
}

CommentaryField.propTypes = {
  CommentaryId: PropTypes.string,
  CommentId: PropTypes.string,
  Languages: PropTypes.objectOf(PropTypes.string),
  NoteId: PropTypes.number,
  Paragraphs: PropTypes.arrayOf(PropTypes.string),
};

export default function CommentaryField (props) {
  // props
  const commentaryId = props.CommentaryId;
  const commentId = props.CommentId;
  const languages = props.Languages;
  const noteId = props.NoteId;
  const paragraphs = props.Paragraphs;

  // states
  const contentState = convert(paragraphs);
  const [editorState, setEditorState] = React.useState(
    contentState
      ? EditorState.createWithContent(contentState, compositeDecorator)
      : EditorState.createEmpty(compositeDecorator)
  );
  const [languageState, setLanguageState] = React.useState("");
  const [linkState, setLinkState] = React.useState(
    {
      showURLInput: false,
      urlValue: "",
    }
  );

  // functions
  const { t } = useTranslation();

  const handleKeyCommand = (command, editorState) => {
    const newState = RichUtils.handleKeyCommand(editorState, command);

    if (newState) {
      setEditorState(newState);
      return "handled";
    }

    return "not-handled";
  };

  const handleInlineStyleChange = (event) => {
    const style = event.currentTarget.value;
    setEditorState(RichUtils.toggleInlineStyle(editorState, inlineStyles[style].draftName));
  };

  // LINK event handlers
  const promptForLink = (event) => {
    event.preventDefault();
    const selection = editorState.getSelection();
    if (!selection.isCollapsed()) {
      const contentState = editorState.getCurrentContent();
      const startKey = editorState.getSelection().getStartKey();
      const startOffset = editorState.getSelection().getStartOffset();
      const blockWithLinkAtBeginning = contentState.getBlockForKey(startKey);
      const linkKey = blockWithLinkAtBeginning.getEntityAt(startOffset);

      let url = "";
      if (linkKey) {
        const linkInstance = contentState.getEntity(linkKey);
        url = linkInstance.getData().url;
      }

      setLinkState({
        showURLInput: true,
        urlValue: url,
      });
    }
  };

  const handleCloseLink = () => setLinkState(prevState => ({
    ...prevState, showURLInput: false,
  }));


  const confirmLink = (event) => {
    event.preventDefault();
    const contentState = editorState.getCurrentContent();
    const contentStateWithEntity = contentState.createEntity(
      "LINK",
      "MUTABLE",
      {url: linkState.urlValue}
    );
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const newEditorState = EditorState.set(editorState, { currentContent: contentStateWithEntity });
    setEditorState(
      RichUtils.toggleLink(
        newEditorState,
        newEditorState.getSelection(),
        entityKey
      ),
    );
    setLinkState(
      {
        showURLInput: false,
        urlValue: "",
      }
    );

  };

  const onLinkInputKeyDown = (event) => {
    if (event.which === 13) {
      confirmLink(event);
    }
  };

  const onURLChange = (event) => {
    setLinkState(prevState => ({
      ...prevState, urlValue : event.target.value,
    }));
  };

  const removeLink = (event) => {
    event.preventDefault();
    const selection = editorState.getSelection();
    if (!selection.isCollapsed()) {
      setEditorState(RichUtils.toggleLink(editorState, selection, null));
    }
  };

  //change language event handlers
  const handleLanguageChange = (event) => {
    event.preventDefault();
    const contentState = editorState.getCurrentContent();
    const contentStateWithEntity = contentState.createEntity(
      "LANGUAGE",
      "MUTABLE",
      {lang: event.target.value}
    );
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const newEditorState = EditorState.set(editorState, { currentContent: contentStateWithEntity });
    setEditorState(
      RichUtils.toggleLink(
        newEditorState,
        newEditorState.getSelection(),
        entityKey
      ),
    );
  };

  const saveComm = async () => {
    const content = editorState.getCurrentContent();
    const html = convertToHTML(conversionMap)(content);
    // array without "<p />"
    const arrayOfP = html.replace(/<p>/g, "").split("</p>");
    // clean up empty array members aka "", e.g. empty lines
    const filteredArrayOfP = arrayOfP.filter(p => p !== "");
    // because the operation is an update/replace, we do not need to "create"
    if (filteredArrayOfP.length > 0) {
      await updateNode(commentaryId, commentId, noteId, NODE_NAME, filteredArrayOfP);
    } else {
      await destroyNode(commentaryId, commentId, noteId, NODE_NAME);
    }
    //TODO wait for success
  };

  //DEBUG
  const logState = () => {
    const content = editorState.getCurrentContent();
    const html = convertToHTML(conversionMap)(content);
    console.debug(html);
    console.dir(convertToRaw(content));
    console.debug(paragraphs);
  };

  return (
    <Stack>
      <Box sx={{border: "1px solid rgba(0, 0, 0, 0.23)", "border-radius": "4px", }}>
        <Editor
          customStyleMap={additionalDraftStyles}
          editorState={editorState}
          handleKeyCommand={handleKeyCommand}
          onChange={setEditorState}
        />
      </Box>
      <Stack direction="row" spacing={1}>
        <ToggleButtonGroup  size="small">
          <Tooltip title={t("editor.bold.tooltip")}>
            <ToggleButton value="bold" onMouseDown={handleInlineStyleChange}><FormatBoldIcon /></ToggleButton>
          </Tooltip>
          <Tooltip title={t("editor.italic.tooltip")}>
            <ToggleButton value="italic" onMouseDown={handleInlineStyleChange}><FormatItalicIcon /></ToggleButton>
          </Tooltip>
          <Tooltip title={t("editor.strikethrough.tooltip")}>
            <ToggleButton value="strikethrough" onMouseDown={handleInlineStyleChange}><FormatStrikethroughIcon /></ToggleButton>
          </Tooltip>
          <Tooltip title={t("editor.underline.tooltip")}>
            <ToggleButton value="underline" onMouseDown={handleInlineStyleChange}><FormatUnderlinedIcon /></ToggleButton>
          </Tooltip>
        </ToggleButtonGroup>
        <Divider orientation="vertical" flexItem />
        <ToggleButtonGroup  size="small">
          {
          //TODO: switch between Add and Edit link
          }

          <Tooltip title={t("editor.addLink.tooltip")}>
            <ToggleButton onMouseDown={promptForLink}><AddLinkIcon /></ToggleButton>
          </Tooltip>
          <Dialog open={linkState.showURLInput} onClose={handleCloseLink} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">{t("editor.linkDialog.title")}</DialogTitle>
            <DialogContent>
              <DialogContentText>
                {t("editor.linkDialog.description")}
              </DialogContentText>
              <TextField
                autoFocus
                margin="dense"
                fullWidth
                name="id"
                label={t("editor.linkDialog.inputLabel")}
                onChange={ onURLChange }
                onKeyDown={ onLinkInputKeyDown }
                value={linkState.urlValue}
                size="small"
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={handleCloseLink} color="primary">
                {t("button.cancel.title")}
              </Button>
              <Button onClick={confirmLink} color="primary" variant="contained">
                {t("editor.linkDialog.title")}
              </Button>
            </DialogActions>
          </Dialog>
          <Tooltip title={t("editor.removeLink.tooltip")}>
            <ToggleButton onMouseDown={removeLink}><LinkOffIcon /></ToggleButton>
          </Tooltip>
        </ToggleButtonGroup>
        <Divider orientation="vertical" flexItem />
        <FormControl size="small" sx={{ width: 120 }}>
          {/* <InputLabel id="demo-simple-select-label">{t("note.language.title")}</InputLabel> */}
          <Select
            defaultValue=""
            id="demo-simple-select"
            value={languageState}
            label="Language"
            labelId="demo-simple-select-label"
            onChange={handleLanguageChange}
          >
            { Object.entries(languages).map(lang =>
              <MenuItem key={lang[0]} value={lang[0]}>{lang[1]}</MenuItem>)
            }
          </Select>
        </FormControl>
        <Button onClick={saveComm}><SaveAlt /></Button>
        <Divider orientation="vertical" flexItem />
        <Button onMouseDown={logState}>LOG</Button>
      </Stack>
    </Stack>
  );
}
