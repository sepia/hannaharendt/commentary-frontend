import React from "react";
import { useTranslation } from "react-i18next";

import { Button, LinearProgress, Tooltip } from "@mui/material";

import DescriptionIcon from "@mui/icons-material/Description";

import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { List, ListItem, ListItemButton, ListItemIcon, ListItemText } from "@mui/material";
import DownloadIcon from "@mui/icons-material/Download";

import { useSnackbar } from "notistack";

import { retrieveCommentaryFilename, retrieveCommentaryIds } from "../api/crud";


export default function DownloadCommentaryCollection() {
  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [values, setValues] = React.useState([""]);

  const { enqueueSnackbar } = useSnackbar();
  const { t } = useTranslation();

  const handleOnChoseFile = async (commentaryCollectionId) => {
    /**
     * temporary solution without using react router
     */
    const fileName = await retrieveCommentaryFilename(commentaryCollectionId);
    window.location.replace(`/download/${fileName}`);
    setOpen(false);
  };

  const handleOpen = async () => {
    setOpen(true);
    setLoading(true);
    try {
      setValues(await retrieveCommentaryIds());
    } catch (error) {
      enqueueSnackbar(error.message, {
        variant: "error",
      });
      return;
    } finally {
      setLoading(false);
    }
  };
  const handleClose = () => setOpen(false);

  return (
    <>
      <Tooltip title={t("commentary.collection.download.description")}>
        <ListItem button key={"open"} aria-label="open edition" onClick={handleOpen}>
          <ListItemIcon>
            <DownloadIcon />
          </ListItemIcon>
          <ListItemText primary={t("commentary.collection.download.title")} />
        </ListItem>
      </Tooltip>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="open-edition-title"
        aria-describedby="open-edition-description"
      >
        <DialogTitle id="open-edition-title">{t("commentary.collection.download.title")}</DialogTitle>
        <DialogContent>
          <DialogContentText id="open-edition-description">{t("commentary.collection.download.description")}</DialogContentText>
          {loading ? (
            <LinearProgress />
          ) : (
            <List>
              {values.map((commentaryCollectionId) => (
                <ListItemButton key={commentaryCollectionId} value={commentaryCollectionId} onClick={() => handleOnChoseFile(commentaryCollectionId)}>
                  <ListItemIcon>
                    <DescriptionIcon />
                  </ListItemIcon>
                  <ListItemText>{commentaryCollectionId}</ListItemText>
                </ListItemButton>
              ))}
            </List>
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            {t("button.cancel.title")}
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
