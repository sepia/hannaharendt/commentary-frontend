import * as React from "react";

import PropTypes from "prop-types";

import { Box, LinearProgress, Tab } from "@mui/material";
import { TabContext, TabList, TabPanel} from "@mui/lab";

import { useSnackbar } from "notistack";

import CommentContainer from "./CommentContainer";

import { retrieveCommentsIds, retrieveEdition, retrieveLanguages} from "../api/crud";


LabTabs.propTypes = {
  EditionId: PropTypes.string,
  CommentaryId: PropTypes.string,
};

export default function LabTabs(props) {
  const commentaryId = props.CommentaryId;
  const editionId = props.EditionId;

  const [comments, setComments] = React.useState([]);
  const [languages, setLanguages] = React.useState({});
  const [loading, setLoading] = React.useState(false);
  const [value, setValue] = React.useState(false); // init with no tab selected; better: `comments[0]` = first item or an empty Tab (TBD)

  const { enqueueSnackbar } = useSnackbar();


  React.useEffect(async () => {
    const languages = await retrieveLanguages(editionId);
    //TODO: check `languages`; if/empty or try/catch
    setLanguages(languages);
  },[]);

  React.useEffect(async () => {
    setLoading(true);
    try {
      // RETURN TO LOADING THE COMMENTS FROM THE comment file because there will be comments that are not yet referenced!!!
      setComments(await retrieveEdition(editionId));
      console.warn(commentaryId);
      //setComments(await retrieveCommentsIds(commentaryId));
    } catch (error) {
      enqueueSnackbar(error.message, {
        variant: "error",
      });
    } finally {
      setLoading(false);
    }
    //TODO: open first Tab
  },[]);

  const handleChange = async (event, newValue) => {
    setValue(newValue);
  };



  return (
    <Box sx={{
      flexGrow: 1,
      flexDirection: "column",
      //flexDirection: 'row',
      bgcolor: "background.paper",
      display: "flex"
    }}>
      <TabContext value={value}>
        <TabList
          onChange={ handleChange }
          aria-label="lab API tabs example"
          variant="scrollable"
          scrollButtons="auto"
          //orientation="vertical"
          //sx={{ borderLeft: 1, borderColor: 'divider' }}
        >
          {
            comments.map(
              comment => <Tab value={ comment } label={ comment } key={ comment } />
            )
          }
        </TabList>
        {loading && <LinearProgress />}
        {
          comments.map(comment =>
            <TabPanel
              value={comment}
              key={comment}
              //sx={{ flexGrow: 1 }}
            >
              <CommentContainer CommentaryId={commentaryId} CommentId={comment} Languages={languages} />
            </TabPanel>
          )
        }
      </TabContext>
    </Box>
  );
}
