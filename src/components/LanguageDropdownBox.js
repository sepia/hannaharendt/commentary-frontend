import React from "react";
import PropTypes from "prop-types";
import { FormControl, InputLabel, LinearProgress, MenuItem, Select, Tooltip } from "@mui/material";
import { waitUntil } from "async-wait-until";
import { useSnackbar } from "notistack";

import  { createNode, retrieveNode, updateNode } from "../api/crud";
import { useTranslation } from "react-i18next";


LanguageDropdownBox.propTypes = {
  CommentaryId: PropTypes.string,
  CommentId: PropTypes.string,
  Languages: PropTypes.objectOf(PropTypes.string),
  NoteId: PropTypes.number,
  Value: PropTypes.string,
};

const NODE_NAME = "lang";


export default function LanguageDropdownBox(props) {
  const commentaryId = props.CommentaryId;
  const commentId = props.CommentId;
  const languages = props.Languages;
  const noteId = props.NoteId;

  const [loading, setLoading] = React.useState(false);
  const [value, setValue] = React.useState(props.Value);

  const { enqueueSnackbar } = useSnackbar();
  const { t } = useTranslation();


  const handleChange =  async (event) => {
    setLoading(true);
    const local_state = event.target.value;

    if (value === "") {
      createNode(commentaryId, commentId, noteId, NODE_NAME, local_state);
    } else {
      updateNode(commentaryId, commentId, noteId, NODE_NAME, local_state);
    }


    console.warn(value +" !== "+ event.target.value);
    setValue(event.target.value);



    try {
      //a single get call directly after post will sometimes be too fast (before the update operation has completed), so wait until the change has propagated but no longer than a set timeout
      const success = await waitUntil(async () => {
        return local_state === (await retrieveNode(commentaryId, commentId, noteId, NODE_NAME));
      });
      console.debug("success? " + success);
      if (success) {
        const remote_state = await retrieveNode(commentaryId, commentId, noteId, NODE_NAME);
        console.debug("remote state: " + JSON.stringify(remote_state));
        setValue(remote_state);
        enqueueSnackbar("Update success.",
          { variant: "success" }
        );
      }
    } catch (error) {
      enqueueSnackbar(error.message, {
        variant: "error",
      });
    } finally {
      setLoading(false);
    }
    //TODO: set the remote state independent of success to keep local and remote in sync (at least for non-text fields)
    //OR, even better, implement a repeat button on fail
  };


  return (
    <>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">{t("note.language.title")}</InputLabel>
        <Tooltip title={t("note.language.tooltip")}>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={value}
            label="Language"
            onChange={handleChange}
          >
            { Object.entries(languages).map(lang =>
              <MenuItem key={lang[0]} value={lang[0]}>{lang[1]}</MenuItem>)
            }
          </Select>
        </Tooltip>
      </FormControl>
      {loading && <LinearProgress />}
    </>
  );
}
