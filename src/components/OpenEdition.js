import React from "react";
import { useTranslation } from "react-i18next";

import PropTypes from "prop-types";

import { Button, LinearProgress, Tooltip } from "@mui/material";

import DescriptionIcon from "@mui/icons-material/Description";

import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { List, ListItem, ListItemButton, ListItemIcon, ListItemText} from "@mui/material";
import FolderOpenIcon from "@mui/icons-material/FolderOpen";

import { useSnackbar } from "notistack";

import { retrieveEditions } from "../api/crud";


OpenEdition.propTypes = {
  OnChoseFile: PropTypes.func,
};

export default function OpenEdition(props) {
  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [values, setValues] = React.useState([""]);

  const { enqueueSnackbar } = useSnackbar();
  const { t } = useTranslation();


  const handleOnChoseFile =  (editionId) => {
    props.OnChoseFile(editionId);
    setOpen(false);
  };

  const handleOpen = async () => {
    setOpen(true);
    setLoading(true);
    try {
      setValues(await retrieveEditions());
    } catch (error) {
      enqueueSnackbar(error.message, {
        variant: "error",
      });
      return;
    } finally {
      setLoading(false);
    }
  };
  const handleClose = () => setOpen(false);


  return (
    <>
      <Tooltip title={t("openEdition.description")}>
        <ListItem
          button
          key={"open"}
          aria-label="open edition"
          onClick={handleOpen}
        >
          <ListItemIcon>
            <FolderOpenIcon />
          </ListItemIcon>
          <ListItemText primary={t("openEdition.title")} />
        </ListItem>
      </Tooltip>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="open-edition-title"
        aria-describedby="open-edition-description"
      >
        <DialogTitle id="open-edition-title">{t("openEdition.title")}</DialogTitle>
        <DialogContent>
          <DialogContentText id="open-edition-description">
            {t("openEdition.description")}
          </DialogContentText>
          {
            loading
              ? <LinearProgress />
              : <List>
                {
                  values.map(
                    editionId =>
                      <ListItemButton key={editionId} value={editionId} onClick={() => handleOnChoseFile(editionId)}>
                        <ListItemIcon><DescriptionIcon /></ListItemIcon>
                        <ListItemText>{editionId}</ListItemText>
                      </ListItemButton>
                  )
                }
              </List>
          }
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            {t("button.cancel.title")}
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}