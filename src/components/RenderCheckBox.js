import React from "react";

import PropTypes from "prop-types";

import LinearProgress from "@mui/material/LinearProgress";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";

import { waitUntil } from "async-wait-until";
import { useSnackbar } from "notistack";

import  { createNode, destroyNode, retrieveNode } from "../api/crud";
import { Tooltip } from "@mui/material";
import { useTranslation } from "react-i18next";


RenderCheckBox.propTypes = {
  CommentaryId: PropTypes.string,
  CommentId: PropTypes.string,
  NoteId: PropTypes.number,
  Value: PropTypes.string,
};



const NODE_NAME = "rend";

export default function RenderCheckBox(props) {
  const commentaryId = props.CommentaryId;
  const commentId = props.CommentId;
  const noteId = props.NoteId;

  const [show, setShow] = React.useState(props.Value);
  const [loading, setLoading] = React.useState(false);

  const { enqueueSnackbar } = useSnackbar();
  const { t } = useTranslation();



  //compare local state with remote state
  const compare = async (data) => {
    const remote_state = await retrieveNode(commentaryId, commentId, noteId, NODE_NAME);
    console.debug(remote_state +"="+ data);
    return remote_state === data;
  };

  const handleChange = async (event) => {
    setLoading(true);
    setShow(event.target.checked);
    const local_state = event.target.checked;
    console.debug("local state: " + local_state);

    if (local_state) {
      await createNode(commentaryId, commentId, noteId, NODE_NAME);
    } else {
      await destroyNode(commentaryId, commentId, noteId, NODE_NAME);
    }

    try {
      //a single get call directly after post will sometimes be too fast (before the update operation has completed), so wait until the change has propagated but no longer than a set timeout
      const success = await waitUntil(() => compare(local_state));
      console.debug("success? " + success);
      if (success) {
        const remote_state = await retrieveNode(commentaryId, commentId, noteId, NODE_NAME);
        console.debug("remote state: " + JSON.stringify(remote_state));
        setShow(remote_state);
        enqueueSnackbar("Update success.",
          { variant: "success" }
        );
      }
    } catch (error) {
      enqueueSnackbar(error.message, {
        variant: "error",
      });
    } finally {
      setLoading(false);
    }
  };



  return (
    <>
      <Tooltip title={t("note.render.tooltip")}>
        <FormControlLabel
          control={
            <Switch
              checked={show}
              disabled={loading}
              onChange={handleChange}
              name="rend"
            />
          }
          label={t("note.render.title")}
          labelPlacement="start"
        />
      </Tooltip>
      {loading && <LinearProgress />}
    </>
  );
}
