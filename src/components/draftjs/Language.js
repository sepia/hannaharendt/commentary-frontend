import React from "react";
import PropTypes from "prop-types";
import { additionalDraftStyles } from "./styles";
import { ContentState } from "draft-js";

const styles = {
  en: {draftName: "RED"},
  de: {draftName: "PINK"},
  "grc-la": {draftName: "SEPIA"},
  grc: {draftName: "BLUE"},
  fr: {draftName: "GREEN"},
  la: {draftName: "ORANGE"},
};

// Strategy
function findLanguageEntities(contentBlock, callback, contentState) {
  contentBlock.findEntityRanges(
    (character) => {
      const entityKey = character.getEntity();
      return (
        entityKey !== null &&
        contentState.getEntity(entityKey).getType() === "LANGUAGE"
      );
    },
    callback
  );
}

// Component
const Language = (props) => {
  const {lang} = props.contentState.getEntity(props.entityKey).getData();
  return (
    <span {...props} lang={lang} style={additionalDraftStyles[styles[lang].draftName]}>
      {props.children}
    </span>
  );
};

Language.propTypes = {
  children: PropTypes.node,
  contentState: PropTypes.instanceOf(ContentState),
  entityKey: PropTypes.string,
};

export {findLanguageEntities, Language};
