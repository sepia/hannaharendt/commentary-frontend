import React from "react";

import PropTypes from "prop-types";

import { additionalDraftStyles } from "./styles";
import { ContentState } from "draft-js";


// Strategy
function findLinkEntities(contentBlock, callback, contentState) {
  contentBlock.findEntityRanges(
    (character) => {
      const entityKey = character.getEntity();
      return (
        entityKey !== null &&
        contentState.getEntity(entityKey).getType() === "LINK"
      );
    },
    callback
  );
}

// Component
const Link = (props) => {
  const {url} = props.contentState.getEntity(props.entityKey).getData();
  return (
    <a {...props} href={url} style={additionalDraftStyles.LINK}>
      {props.children}
    </a>
  );
};

Link.propTypes = {
  children: PropTypes.node,
  contentState: PropTypes.instanceOf(ContentState),
  entityKey: PropTypes.string,
};

export {findLinkEntities, Link};
