export default class ExistsError extends Error {
  constructor(element, ...params) {
    super(...params);

    this.name = "ExistsError";
    this.element = element;
    this.message = `Element ${element} already exists.`;
  }
}
