export default class NotfoundError extends Error {
  constructor(element, ...params) {
    super(...params);

    this.name = "NotfoundError";
    this.element = element;
    this.message = `Element ${element} not found.`;
  }
}
