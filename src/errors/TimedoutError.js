export default class TimedoutError extends Error {
  constructor(timeout, ...params) {
    super(...params);

    this.name = "TimedoutError";
    this.timeout = timeout;
    this.message = `Connection timed out (${this.timeout} ms).`;
  }
}
