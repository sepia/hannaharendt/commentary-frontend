import i18n from "i18next";
import { initReactI18next } from "react-i18next";

export default i18n
  // pass the i18n instance to react-i18next.
  .use(initReactI18next)
  // init i18next
  // for all options read: https://www.i18next.com/overview/configuration-options
  .init({
    debug: true,
    fallbackLng: "en",
    interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
    },
    resources: {
      en: {
        translation: {
          bibContainer: {
            inputLabel: {
              corresp: "Bibliographic Reference",
              from: "Starting Page Number",
              internal: "Internal Notes",
              n: "Additional Reference",
              reference: "Alternative Reference",
              scope: "Page Number (Non-Numerical)",
              to: "Ending Page Number",
            },
            tooltip: {
              corresp: "Reference-ID to external bibliographic data, e.g. “LessingBeweisGELW”.",
              from: "Starting page number of quotation. If single page number, only use this field.",
              internal: "Internal notes will not appear in the commentary.",
              n: "Short reference printed in addition to page number, e.g. for scholarly notations.",
              reference: "Non-structured, alternative reference printed in place of bibliographical reference.",
              scope: "Page number format, containing letters, e.g. “XIV” or “337-338 und 340”.",
              to: "Ending page number of quotation. If single page number, only use “Starting Page Number”.",
            },
          },
          button: {
            cancel: {
              title: "Cancel",
            },
          },
          comment: {
            add: {
              description: "Enter an ID, or leave the field empty to receive a random ID.",
              inputLabel: "id",
              title: "Add comment",
              tooltip: "Add a comment to the edition.",
            },
          },
          commentary: {
            collection: {
              add: {
                description: "",
                inputLabel: "id",
                title: "Add commentary collection",
                tooltip: "Add a commentary collection to the edition.",
              },
              download: {
                description: "Download a commentary collection.",
                init: "No edition loaded.",
                title: "Download commentary collection",
              },
            },
            item: {
              add: {
                description: "Enter an ID, or leave the field empty to receive a random ID.",
                inputLabel: "id",
                title: "Add commentary item",
                tooltip: "Add a commentary item to the collection.",
              },
            },
          },
          note: {
            add: {
              tooltip: "Add a note to the commentary.",
            },
            delete: {
              tooltip: "Delete this note from the commentary.",
            },
            language: {
              title: "Language",
              tooltip: "L",
            },
            render: {
              title: "Render",
              tooltip: "R",
            },
            additional: {
              title: "Additional",
              tooltip: "A",
            },
          },
          openEdition: {
            description: "Load the commentaries of a certain edition.",
            init: "No edition loaded.",
            title: "Open edition",
          },
          editor: {
            addLink: {
              tooltip: "Add a link to the selected text.",
            },
            bold: {
              tooltip: "Bold.",
            },
            editLink: {
              tooltip: "Edit link",
            },
            italic: {
              tooltip: "Italic.",
            },
            linkDialog: {
              title: "Add link",
              description: "",
              inputLabel: "",
            },
            removeLink: {
              tooltip: "Remove a link from the selected text.",
            },
            strikethrough: {
              tooltip: "Strike-through.",
            },
            underline: {
              tooltip: "Underline.",
            },
          },
        },
      },
    },
  });
