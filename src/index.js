import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
//import reportWebVitals from "./reportWebVitals";

import CssBaseline from "@mui/material/CssBaseline";
import { ThemeProvider, StyledEngineProvider } from "@mui/material/styles";

import { SnackbarProvider } from "notistack";

import * as Sentry from "@sentry/react";
import { BrowserTracing } from "@sentry/tracing";

import "./i18n";
import theme from "./theme";

Sentry.init({
  dsn: "https://0eced1488027417d9f34e30de23c3ae6@errs.sub.uni-goettingen.de/11",
  integrations: [new BrowserTracing()],
  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,
});

ReactDOM.render(
  <React.StrictMode>
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <SnackbarProvider>
          <App />
        </SnackbarProvider>
      </ThemeProvider>
    </StyledEngineProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

// The in-browser Sentry SDKs collect Web Vitals information (where supported) and add that information to frontend transactions.
// reportWebVitals();
