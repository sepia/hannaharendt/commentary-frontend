import { red } from "@mui/material/colors";
import { createTheme } from "@mui/material/styles";


const theme = createTheme({
  palette: {
    primary: {
      main: "#704214",
    },
    secondary: {
      main: "#8fbdeb",
    },
    error: {
      main: red.A400,
    },
  },
});

export default theme;
